/*
 * Copyright (C) 2016 José Bollo <jobol@nonadev.net>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, version 2.
 *
 * Author:
 *      José Bollo <jobol@nonadev.net>
 */

#if defined(WITHOUT_USERNS)
#  if defined(WITH_USERNS)
#    error "don't define WITH_USERNS and WITHOUT_USERNS together"
#  endif
#else
#  if !defined(WITH_USERNS)
#    error "you should define either WITH_USERNS or WITHOUT_USERNS"
#  endif
#  define CONFIG_SECURITY_PTAGS_WITH_USER_NS
#endif


#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <pthread.h>

/*****************************************************************************
 * section: emulation of the kernel
 ****************************************************************************/

/* memory */
#define GFP_KERNEL 0
#define __GFP_ZERO 1

#define kmalloc(x,y)    (((y)&__GFP_ZERO)?calloc(1,x):malloc(x))
#define krealloc(p,x,y) realloc(p,x)
#define kfree(x)        free(x)

/* mutexes */
struct mutex { pthread_mutex_t m; };
#define DEFINE_MUTEX(x) struct mutex x = { .m = PTHREAD_MUTEX_INITIALIZER }
#define mutex_init(x)   pthread_mutex_init(&((x)->m),NULL)
#define mutex_lock(x)   pthread_mutex_lock(&((x)->m))
#define mutex_unlock(x) pthread_mutex_unlock(&((x)->m))

/* atomic */
#define atomic_t                int
#define atomic_inc_return(p)    __atomic_add_fetch(p,1,__ATOMIC_RELAXED)
#define atomic_inc(p)           ((void)atomic_inc_return(p))
#define atomic_dec_and_test(p)  (!__atomic_sub_fetch(p,1,__ATOMIC_RELAXED))
#define atomic_read(p)          __atomic_load_n(p,__ATOMIC_RELAXED)
#define atomic_set(p,v)		__atomic_store_n(p,v,__ATOMIC_RELAXED)

#if defined(CONFIG_SECURITY_PTAGS_WITH_USER_NS)
struct user_namespace {
	atomic_t count;
	atomic_t weak_count;
	struct user_namespace *parent;
};

static void free_user_ns(struct user_namespace *ns);

static inline struct user_namespace *get_user_ns(struct user_namespace *ns)
{
	if (ns)
		atomic_inc(&ns->count);
	return ns;
}

static inline void put_user_ns(struct user_namespace *ns)
{
	if (ns && atomic_dec_and_test(&ns->count))
		free_user_ns(ns);
}


static inline struct user_namespace *get_weak_user_ns(struct user_namespace *ns)
{
	if (ns && atomic_inc_return(&ns->weak_count) == 1)
		atomic_inc(&ns->count);
	return ns;
}

static inline void put_weak_user_ns(struct user_namespace *ns)
{
	if (ns && atomic_dec_and_test(&ns->weak_count) && atomic_dec_and_test(&ns->count))
		free_user_ns(ns);
}

static inline int is_weak_user_ns_still_alive(struct user_namespace *ns)
{
	return ns && atomic_read(&ns->count) > 1;
}

static void free_user_ns(struct user_namespace *ns)
{
	struct user_namespace *p = ns->parent;
	free(ns);
	put_weak_user_ns(p);
}

static struct user_namespace init_user_ns = {
	.count = 1,
	.weak_count = 0,
	.parent = NULL
};

static _Thread_local struct user_namespace *_current_user_ns_;

static inline struct user_namespace *current_user_ns(void)
{
	return _current_user_ns_;
}
#endif

/*****************************************************************************
 * section: include the ptags part
 ****************************************************************************/

#include "ptags.c"

/*****************************************************************************
 * section: local needs for namespaces
 ****************************************************************************/

#if defined(CONFIG_SECURITY_PTAGS_WITH_USER_NS)
static inline struct user_namespace *new_child_user_ns(struct user_namespace *parent)
{
	struct user_namespace *ns;

	ns = malloc(sizeof *ns);
	ns->parent = get_weak_user_ns(parent);
	atomic_set(&ns->count, 1);
	atomic_set(&ns->weak_count, 0);
	return ns;
}

static inline void set_current_user_ns(struct user_namespace *ns)
{
	_current_user_ns_ = ns;
}
#endif

/*****************************************************************************
 * section: toolbox functions
 ****************************************************************************/

/* print the content of a ptags */
void do_read(struct ptags *c)
{
	int l;
	char *d = NULL;

	l = ptags_read(c, &d);
	if (l < 0) {
		errno = -l;
		printf("read %d %s\n\n", l, strerror(errno));
	} else {
		printf("read %d\n---\n%.*s===\n\n", l, l, d);
		free(d);
	}
}

void do_write(struct ptags *c, struct ptags *m, const char *data)
{
	int r;
	size_t l;

	l = strlen(data);
	r = ptags_write(c, m, data, l);
	if (r < 0) {
		errno = -r;
		printf("write %d %s\n---\n%s===\n\n", r, strerror(errno), data);
	} else {
		printf("write %d\n---\n%s===\n\n", r, data);
	}
}

/* returns a copy of 'a' */
struct ptags *twin(struct ptags *a)
{
	int i;
	struct ptags *b;
	b = ptags_create();
	assert(b);
	i = ptags_copy(b, a);
	assert(i == 0);
	return b;
}

/* returns 'a' pruned */
struct ptags *prune(struct ptags *a)
{
	ptags_prune(a);
	return a;
}

void vadd(struct ptags *a, va_list ap)
{
	int n;
	char *p;
	const char *data = va_arg(ap, const char *);
	while(data) {
		n = asprintf(&p, "+%s\n", data);
		assert(n > 0);
		n = ptags_write(NULL, a, p, (size_t)n);
		assert(n > 0);
		free(p);
		data = va_arg(ap, const char *);
	}
}

void add(struct ptags *p, ...)
{
	va_list ap;
	va_start(ap, p);
	vadd(p, ap);
	va_end(ap);
}

struct ptags *init(struct ptags *c, ...)
{
	struct ptags *r;
	va_list ap;

	r = ptags_create();
	assert(r);
	if (c)
		ptags_copy(r, c);
	va_start(ap, c);
	vadd(r, ap);
	va_end(ap);
	return r;
}

void vtests(struct ptags *c, struct ptags *m, int f, va_list ap)
{
	int i = 0;
	const char *data;

	printf("##################### BEGIN TEST SET\n");
	if (c) {
		printf("###### CONTROL ######\n");
		printf("#####################\n");
		do_read(c);
	} else {
		printf("##### NO CONTROL ####\n");
	}
	printf("#####################\n");

	data = va_arg(ap, const char *);
	while(data) {
		printf("### %d ### - - - - - - - - - - -\n", ++i);
		if (m) {
			do_write(c, m, data);
			do_read(m);
		} else {
			do_write(c, c, data);
			do_read(c);
		}
		data = va_arg(ap, const char *);
	}
	printf("##################### END TEST SET\n\n");
	if (f) {
		ptags_free(c);
		if (m != c)
			ptags_free(m);
	}
}

void tests(struct ptags *c, struct ptags *m, ...)
{
	va_list ap;
	va_start(ap, m);
	vtests(c, m, 1, ap);
	va_end(ap);
}

void testsnf(struct ptags *c, struct ptags *m, ...)
{
	va_list ap;
	va_start(ap, m);
	vtests(c, m, 0, ap);
	va_end(ap);
}

/*****************************************************************************
 * section: tests
 ****************************************************************************/

/* test creation & destruction */
void test1(void)
{
	struct ptags *a = ptags_create();
	do_read(a);
	ptags_free(a);
}

/* test creation & simple writes & destruction */
void test2(void)
{
	tests(NULL, init(NULL, NULL),
		"",
		"# this is a comment\n",
		"this is an unterminated line",
		"the command is not known\n",
		"+this.is=.an.invalid.tag\n",
		"+this.is*.an.invalid.tag\n",
		"+this is a valid tag\n",
		"?this is a valid tag\n",
		"?@this is a valid tag\n",
		"+@this is a valid tag\n",
		"?this is a valid tag\n",
		"?@this is a valid tag\n",
		"!this is a valid tag=nothing\n",
		"-@this is a valid tag\n",
		"!this is a valid tag=no more nothing\n",
		"!this is a valid tag\n",
		"-this is a valid tag\nwith unterminated line",
		NULL);
}

/* test copy, move and prune */
void test3(void)
{
	struct ptags *a = init(NULL, "a", "a:a", "@a:b", "@b:c", NULL);
	struct ptags *b = init(NULL, NULL);
	struct ptags *c = init(NULL, NULL);
	struct ptags *d = init(NULL, NULL);
	ptags_copy(b, a);
	ptags_copy(c, b);
	ptags_move(d, a);
	ptags_prune(b);
	do_read(a);
	do_read(b);
	do_read(c);
	do_read(d);
	ptags_free(a);
	ptags_free(b);
	ptags_free(c);
	ptags_free(d);
}

/* test prefix */
void test4(void)
{
	tests(NULL, init(NULL, NULL),
		"+@:x\n+a\n+a:a\n+@a:b\n+@b:c\n",
		"-@:*\n",
		"-@\n",
		"?@\n",
		"?a:*\n",
		"-a:*\n",
		"?a:*\n",
		"-\n",
		NULL);
}

/* test own adds */
void test5(void)
{
	tests(
		init(
			NULL,
			"ptags:ptags:add",
			"ptags:ptags:sub",
			"ptags:c:add",
			"ptags:a:sub",
			"ptags:b:set",
			"a",
			"b",
			"c",
			"a:a",
			"a:b",
			"@a:c",
			"b:x",
			NULL),
		NULL,
		"+c:x\n# allowed by ptags:c:add\n",
		"-c:x\n# not allowed\n",
		"+@c:x\n# allowed by ptags:c:add\n",
		"!c:x=T\n# not allowed\n",
		"!b:x=T\n# allowed by ptags:b:set\n",
		"-@a:c\n#allowed by ptags:a:sub\n",
		"-a:c\n#allowed by ptags:a:sub\n",
		"+a:c\n#not allowed\n",
		"+x\n# not allowed !\n",
		"+ptags:add\n# allowed by ptags:ptags:add\n",
		"+x\n# allowed now by ptags:add\n",
		"-ptags:ptags:*\n# allowed by ptags:ptags:sub\n",
		"-\n",
		NULL);
}


/* test others */
void test6(void)
{
	tests(init(NULL, "ptags:add", "ptags:sub", "ptags:set", NULL),
		init(NULL, "ptags:add", "ptags:sub", "ptags:set", "a", NULL),
		"!a=b\n# not allowed\n",
		"-a\n# not allowed\n",
		"+b\n# not allowed\n",
		"+a\n# allowed\n",
		"-b\n# allowed\n",
		NULL);
	tests(init(NULL, "ptags:set", "ptags:others", NULL),
		init(NULL, "a", "ptags:others", NULL),
		"!a=b\n# allowed\n",
		"-a\n# not allowed\n",
		"+b\n# not allowed\n",
		NULL);
	tests(init(NULL, "ptags:sub", "ptags:others", NULL),
		init(NULL, "a", "ptags:sub", NULL),
		"!a=b\n# not allowed\n",
		"-a\n# allowed\n",
		"+b\n# not allowed\n",
		"-ptags:sub\n# not allowed\n",
		NULL);
	tests(init(NULL, "ptags:add", "ptags:others", NULL),
		init(NULL, "a", "ptags:add", NULL),
		"!a=b\n# not allowed\n",
		"-a\n# not allowed\n",
		"+b\n# allowed\n",
		"+ptags:sub\n# not allowed\n",
		NULL);
	tests(init(NULL, "ptags:add", "ptags:sub", "ptags:set", "ptags:x:others", NULL),
		init(NULL, "a", NULL),
		"-a\n# not allowed\n",
		"+b\n# not allowed\n",
		"!a=e\n# not allowed\n",
		"+x:a\n# allowed\n",
		"!x:a=e\n# allowed\n",
		"-x:a\n# allowed\n",
		NULL);
}

#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
/* test others */
void test7(void)
{
	struct user_namespace *root, *br1, *br2, *br11;
	struct ptags *p, *r;

	root = current_user_ns();
	br1 = new_child_user_ns(root);
	br11 = new_child_user_ns(br1);
	br2 = new_child_user_ns(root);

	p = init(NULL, "ptags:add", "ptags:sub", "ptags:set", "ptags:others", NULL);
	set_current_user_ns(br1);
	r = init(p, "a:a", "a:b", "a:c", "b:a", "b:b", "b:c", NULL);
	do_read(r);
	set_current_user_ns(br2);
	testsnf(NULL, r, "-ptags:set\n-ptags:add\n", NULL);
	set_current_user_ns(br11);
	testsnf(p, r, "-a:*\n+@\n", NULL);
	set_current_user_ns(br1);
	do_read(r);
	set_current_user_ns(root);
	do_read(r);
	set_current_user_ns(br2);
	do_read(r);
	set_current_user_ns(br11);
	do_read(r);
	set_current_user_ns(root);
	put_user_ns(br11);
	set_current_user_ns(br1);
	do_read(r);
	set_current_user_ns(root);
	do_read(r);
	set_current_user_ns(br2);
	do_read(r);
	set_current_user_ns(root);
	put_user_ns(br1);
	do_read(r);
	set_current_user_ns(br2);
	do_read(r);
	set_current_user_ns(root);
	put_user_ns(br2);
	do_read(r);
	ptags_free(r);
	ptags_free(p);
}
#endif

static void stress(int count, int groups, int repeat);

int main(int ac, char **av)
{
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
	set_current_user_ns(&init_user_ns);
#endif
	test1();
	test2();
	test3();
	test4();
	test5();
	test6();
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
	test7();
#endif
	stress(256, 8, 3);
	return 0;
}

/*****************************************************************************
 * section: STRESS
 ****************************************************************************/


#if defined(CONFIG_SECURITY_PTAGS_WITH_USER_NS)
#endif

struct buff {
	size_t length;
	char *value;
};

static int buffscount;
static struct buff *buffs;

static void make_buff(struct buff *b, const char *name, char op, char kept, const char *value)
{
	char *v;
	size_t ln = strlen(name);
	size_t lv = value ? strlen(value) : 0;

	b->length = (size_t)1 + (size_t)!!kept + ln + (size_t)!!value + lv + (size_t)1;
	v = malloc(b->length);
	assert(v);
	b->value = v;
	*v++ = op;
	if (kept) *v++ = '@';
	memcpy(v, name, ln);
	v += ln;
	if (value) {
		*v++ = '=';
		memcpy(v, value, lv);
		v += lv;
	}
	*v = '\n';
}

static void init_buffs(int count, unsigned seed)
{
	int i, b;
	char name[100];

	buffscount = 8 * count;
	buffs = malloc((unsigned)buffscount * sizeof *buffs);
	assert(buffs);
	for (i = b = 0; i < count; i++)
	{
		sprintf (name, "%d:%.*s", i, (int) (1 + (15 & rand_r(&seed))),
				"0123456789abcdef");
		make_buff(&buffs[b++], name, '+', 0, NULL);
		make_buff(&buffs[b++], name, '+', '@', NULL);
		make_buff(&buffs[b++], name, '-', 0, NULL);
		make_buff(&buffs[b++], name, '-', '@', NULL);
		make_buff(&buffs[b++], name, '?', 0, NULL);
		make_buff(&buffs[b++], name, '?', '@', NULL);
		make_buff(&buffs[b++], name, '!', 0, NULL);
		make_buff(&buffs[b++], name, '!', 0, "something");
	}
	assert(b == buffscount);
}

static void free_buffs(void)
{
	while(buffscount--)
		free(buffs[buffscount].value);
	free(buffs);
}

#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
struct ns {
	struct ns *parent;
	struct user_namespace *ns;
	int count, decount;
	pthread_mutex_t lock;
};

static int nsscount;
static struct ns *nss;

static void init_nss(int count, int groups)
{
	int i, j;

	nsscount = 1 + count / groups;
	nss = malloc((unsigned)nsscount * sizeof *nss);
	assert(nss);
	for (i = 0 ; i < nsscount ; i++) {
		j = i / groups;
		if (j) {
			nss[i].parent = &nss[j];
			nss[i].ns = new_child_user_ns(nss[j].ns);
		} else {
			nss[i].parent = NULL;
			nss[i].ns = new_child_user_ns(&init_user_ns);
		}
		assert(nss[i].ns);
		nss[i].decount = nss[i].count = groups;
		pthread_mutex_init(&nss[i].lock, NULL);
	}
}

static void free_nss(void)
{
	while(nsscount--) {
		put_user_ns(nss[nsscount].ns);
	}
	free(nss);
}

static struct user_namespace *nsget(struct ns *ns)
{
	struct user_namespace *r;
	pthread_mutex_lock(&ns->lock);
	r = ns->ns;
	if (!--ns->decount) {
		put_user_ns(r);
		ns->ns = r = new_child_user_ns(ns->parent ? ns->parent->ns : &init_user_ns);
		assert(ns->ns);
		ns->decount = ns->count;
	}
	r = get_user_ns(r);
	pthread_mutex_unlock(&ns->lock);
	return r;
}
#endif

struct proc {
	struct proc *parent;
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
	struct ns *ns;
#endif
	struct ptags *ptags;
	pthread_t tid;
	pthread_rwlock_t rwlock;
	unsigned seed;
};

static int procscount;
static struct proc *procs;

static void init_procs(int count, int groups)
{
	int i, j;

	procscount = count;
	procs = malloc((unsigned)count * sizeof *procs);
	assert(procs);

	for (i = 0 ; i < count ; i++) {
		j = i / groups;
		procs[i].parent = j ? &procs[j-1] : NULL;
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
		procs[i].ns = &nss[j];
#endif
		procs[i].ptags = init(NULL, "@ptags:add", "@ptags:sub", "@ptags:set", "@ptags:others", NULL);
		procs[i].seed = (unsigned)(1000 + 5 * i);
	}
}

static void free_procs(void)
{
	while(procscount--) {
		ptags_free(procs[procscount].ptags);
	}
	free(procs);
}


static void load(struct proc *proc, int count)
{
	struct proc *p;
	struct buff *b;
	while (count--) {
		p = &procs[rand_r(&proc->seed) % procscount];
		b = &buffs[rand_r(&proc->seed) % buffscount];
		pthread_rwlock_rdlock(&proc->rwlock);
		pthread_rwlock_rdlock(&p->rwlock);
		ptags_write(proc->ptags, p->ptags, b->value, b->length);
		pthread_rwlock_unlock(&p->rwlock);
		pthread_rwlock_unlock(&proc->rwlock);
		pthread_yield();
	}
}

static void *runproc(void *arg)
{
	int n, c;
	struct proc *proc = arg;
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
	struct user_namespace *ns;

	set_current_user_ns(&init_user_ns);
#endif
	pthread_rwlock_wrlock(&proc->rwlock);
	n = 5 + (rand_r(&proc->seed) % 5);
	while (n--) {
		/* ensure the tags */
		if (proc->parent) {
			/* recreate the tags as if forked */
			ptags_free(proc->ptags);
			proc->ptags = ptags_create();
			pthread_rwlock_rdlock(&proc->parent->rwlock);
			ptags_copy(proc->ptags, proc->parent->ptags);
			pthread_rwlock_unlock(&proc->parent->rwlock);
			ptags_prune(proc->ptags);
		}

#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
		ns = nsget(proc->ns);
		set_current_user_ns(ns);
#endif
		c = 1000 + (rand_r(&proc->seed) % 100);
		pthread_rwlock_unlock(&proc->rwlock);
		load(proc, c);
		pthread_rwlock_wrlock(&proc->rwlock);
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
		set_current_user_ns(&init_user_ns);
		put_user_ns(ns);
#endif
	}
	pthread_rwlock_unlock(&proc->rwlock);
	pthread_exit(arg);
}

static void runstress(void)
{
	int i;
	pthread_rwlockattr_t attr;

	/* dont care about recursive read locks */
	pthread_rwlockattr_init(&attr);
	pthread_rwlockattr_setkind_np(&attr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);

	/* launches threads */
	for (i = 0 ; i < procscount ; i++) {
		pthread_rwlock_init(&procs[i].rwlock, &attr);
		pthread_rwlock_wrlock(&procs[i].rwlock);
		pthread_create(&procs[i].tid, NULL, runproc, &procs[i]);
	}
	/* runit now: unlock it */
	for (i = 0 ; i < procscount ; i++)
		pthread_rwlock_unlock(&procs[i].rwlock);

	/* wait completion of all threads */
	for (i = 0 ; i < procscount ; i++)
		pthread_join(procs[i].tid, NULL);
}

static void stress(int count, int groups, int repeat)
{
	init_buffs(50, 456123);
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
	init_nss(count, groups);
#endif
	init_procs(count, groups);
	while(repeat-- > 0)
		runstress();
	free_procs();
#ifdef CONFIG_SECURITY_PTAGS_WITH_USER_NS
	free_nss();
#endif
	free_buffs();
}


