/*
MIT License

Copyright (c) 2016  jobol@nonadev.net

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#define _GNU_SOURCE

#include <assert.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdarg.h>

#include "ptags.h"

/* TODO: what about threads and TID?  */

#define GROWING_BUFFER_SIZE 1000
#define MAXIMAL_BUFFER_SIZE 10000000

/* pattern for tag file of processes */
#if !defined(PTAGS_FILE_PATTERN)
#define PTAGS_FILE_PATTERN "/proc/%d/attr/ptags"
#endif
static const char _filepattern_[] = PTAGS_FILE_PATTERN;

/* maximal file length (enougth for 64 bits pids!) */
#define TAGSFILE_MAXLENGTH  (21 + sizeof _filepattern_)

/* structure for recording data */
struct ptags
{
  /* link to the next structure */
  struct ptags *next;

  /* records the pid */
  pid_t pid;

  /* use count */
  int use;

  /* callback definition */
  struct ptags_cbdef cbdef;

  /* watch descriptor returned by inotify */
  int wd;

  /* effective length of tags or -1 when invalidated */
  int length;

  /* allocated size for buffer */
  int size;

  /* buffer for tags of pid */
  char *buffer;

  /* filename for the pid */
  char file[TAGSFILE_MAXLENGTH];
};

/* common structure for unwatched processes and head of watching list */
static struct ptags _ptags_;

/* for notification of watched processes */
static int _watchfd_ = 0;

/* safer has small overhead */
#if !defined(NOT_MULTITHREAD_SAFE)
#include <pthread.h>
static pthread_mutex_t _lock_ = PTHREAD_MUTEX_INITIALIZER;
#endif

/* set errno to 'value' and returns -1 */
static int
_error_ (int value)
{
  errno = value;
  return -1;
}

/* set errno to 'value' and returns NULL */
static void *
_null_ (int value)
{
  errno = value;
  return NULL;
}

/* reads the file 'fd' in the reallocated 'buffer'
   of reallocated 'size'.
   errno:
    - EFBIG      ptags file too big
    - ENOMEM     out of memory
    - EILSEQ     bad sequence of character
*/
static inline int
_readfd_ (int *size, char **buffer, int fd)
{
  int pos, rc, sz, len;
  char *buf;

  assert (size != NULL);
  assert (buffer != NULL);
  assert (*size >= 0);
  assert ((*size == 0) == (*buffer == NULL));

  pos = 0;
  sz = *size;
  buf = *buffer;
  for (;;)
    {
      /* grows if needed */
      len = sz - pos;
      if (len == 0)
	{
	  len = GROWING_BUFFER_SIZE;
	  sz += len;
	  if (sz > MAXIMAL_BUFFER_SIZE)
	    return _error_ (EFBIG);
	  buf = realloc (buf, sz);
	  if (buf == NULL)
	    return _error_ (ENOMEM);
	  *size = sz;
	  *buffer = buf;
	}

      /* reads data */
      rc = (int) read (fd, &buf[pos], len);
      if (rc >= 0)
	{
	  pos += rc;
	  if (rc < len)
	    {
	      if (pos > 0 && buf[pos - 1] != '\n')
		return _error_ (EILSEQ);
	      return pos;
	    }
	}
      else if (errno != EINTR)
	{
	  assert (0);		/* should not happen */
	  return rc;
	}
    }
}

/* reads the file of 'path' in the reallocated 'buffer'
   of reallocated 'size'
   errno:
    - EFBIG      ptags file too big
    - ENOMEM     out of memory
    - EILSEQ     bad sequence of character
    - EACCES     access to ptags not allowed
    - ENOENT     no ptags files exist
    - EMFILE     too many files opened
*/
static inline int
_readfile_ (int *size, char **buffer, const char *path)
{
  int rc;
  do
    {
      rc = open (path, O_RDONLY | O_CLOEXEC);
    }
  while (rc < 0 && errno == EINTR);
  if (rc >= 0)
    {
      int fd = rc;
      rc = _readfd_ (size, buffer, fd);
      close (fd);
    }
  return rc;
}

/* set in 'filename' the path for the tag file of 'pid' */
static inline void
_path_ (char filename[TAGSFILE_MAXLENGTH], pid_t pid)
{
  sprintf (filename, _filepattern_, (unsigned) pid);
}

/* search and returns the watched ptags of 'wd' */
static struct ptags *
_wd_2_ptags_ (int wd)
{
  struct ptags *pt;
  for (pt = _ptags_.next; pt && pt->wd != wd; pt = pt->next);
  return pt;
}

/* search and returns the watched ptags of 'pid' */
static struct ptags *
_pid_2_ptags_ (int pid)
{
  struct ptags *pt;
  for (pt = _ptags_.next; pt && pt->pid != pid; pt = pt->next);
  return pt;
}

/* update the content of tags for 'pt' IF NEEDED
   returns either 'pt' or NULL when needed reading failed
   errno:
    - EFBIG      ptags file too big
    - ENOMEM     out of memory
    - EILSEQ     bad sequence of character
    - EACCES     access to ptags not allowed
    - ENOENT     no ptags files exist
    - EMFILE     too many files opened
*/
static struct ptags *
_update_ (struct ptags *pt)
{
  if (pt->length >= 0)
    return pt;
  pt->length = _readfile_ (&pt->size, &pt->buffer, pt->file);
  if (pt->length >= 0)
    return pt;
  return NULL;
}

/* get the up-to-date ptags for 'wd'
   errno:
    - EFBIG      ptags file too big
    - ENOMEM     out of memory
    - EILSEQ     bad sequence of character
    - EACCES     access to ptags not allowed
    - ENOENT     no ptags files exist
    - EMFILE     too many files opened
    - EBADF      bad watch descriptor 'wd'
*/
static struct ptags *
_look_wd_ (int wd)
{
  struct ptags *pt = _wd_2_ptags_ (wd);
  if (pt != NULL)
    return _update_ (pt);
  return _null_ (EBADF);
}

/* get the up-to-date ptags for 'pid'
   errno:
    - EFBIG      ptags file too big
    - ENOMEM     out of memory
    - EILSEQ     bad sequence of character
    - EACCES     access to ptags not allowed
    - ENOENT     no ptags files exist
    - EMFILE     too many files opened
*/
static struct ptags *
_look_pid_ (pid_t pid)
{
  struct ptags *pt;

  /* solve auto pid */
  if (pid == 0)
    pid = getpid ();

  /* search in watched ptags */
  pt = _pid_2_ptags_ (pid);
  if (pt == NULL)
    {
      /* not in watched ptags, use the global one */
      if (_ptags_.pid != pid)
	{
	  /* the global one is changing */
	  _ptags_.pid = pid;
	  _path_ (_ptags_.file, pid);
	  _ptags_.length = -1;
	}
      pt = &_ptags_;
    }
  return _update_ (pt);
}

/* checks wether the 'buffer' of 'length' has the 'tag'
   returns 1 if it has and 0 if it has not. */
static inline int
_hasf_ (int length, const char *buffer, const char *tag)
{
  int i, n, g;

  for (n = 0; tag[n] && tag[n] != '\n'; n++);
  g = n > 1 && tag[n - 2] == ':' && tag[n - 1] == '*';
  n -= g;
  i = 0;
  while (length)
    {
      if (buffer[0] == '@')
	{
	  buffer++;
	  length--;
	}
      for (i = 0;; i++)
	{
	  assert (i < length);
	  if (i == n)
	    {
	      if (g || buffer[i] == '\n')
		return 1;
	      i++;
	      break;
	    }
	  if (tag[i] != buffer[i])
	    break;
	}
      assert (i < length);
      while (buffer[i++] != '\n')
	assert (i < length);
      assert (i <= length);
      buffer += i;
      length -= i;
    }

  return 0;
}

/* checks wether the 'buffer' of 'length' has the 'tag'
   returns 1 if it has and 0 if it has not. */
static inline int
_has_ (int length, const char *buffer, const char *tag)
{
  return _hasf_ (length, buffer, tag);
}

/* checks the ptags 'pt' wether 'count' of 'tags' are matching 'value'
   for _has_. Returns either:
     * -1 if pt is NULL
     * value if _has_ returned it for all tags
     * the first value of _has_ different of value.

   HACK: for implementing has_all, use value == 1 and for implementing 
   has_any, use value == 0 */
static int
_all_ (struct ptags *pt, int value, int count, const char *const *tags)
{
  int result = pt ? value : -1;
  int i = 0;
  while (result == value && i < count)
    {
      result = _has_ (pt->length, pt->buffer, tags[i]);
      i++;
    }
  return result;
}

/* Ensure the existing of the inotify fd '_watchfd_'
   Returns the file descriptor or -1 in case of error.
   errno:
    - ENOMEM     out of memory
    - EMFILE     too many files opened
*/
static int
_watch_ ()
{
  int rc;

  rc = _watchfd_;
  if (rc == 0)
    {
      rc = inotify_init1 (IN_NONBLOCK | IN_CLOEXEC);
      if (rc == 0)
	{
	  rc = dup (rc);
	  close (0);
	}
      if (rc > 0)
	_watchfd_ = rc;
    }
  return rc;
}

/* creates the watched ptags for 'pid' and returns it
   or NULL on error.
   errno:
    - ENOMEM     out of memory
    - EMFILE     too many files opened
    - EACCES     access to ptags not allowed
    - ENOENT     no ptags files exist
    - ENOSPC     no more space for watching
*/
static struct ptags *
_create_ (pid_t pid)
{
  int rc;
  struct ptags *pt;

  /* ensure watching */
  rc = _watch_ ();
  if (rc < 0)
    return NULL;

  /* allocation */
  pt = malloc (sizeof *pt);
  if (pt == NULL)
    return _null_ (ENOMEM);

  /* ask to be notified for the watcher */
  _path_ (pt->file, pid);
  rc = inotify_add_watch (rc, pt->file, IN_MODIFY);
  if (rc < 0)
    {
      free (pt);
      return NULL;
    }

  /* fullfill */
  pt->pid = pid;
  pt->use = 1;
  pt->cbdef.function = NULL;
  pt->cbdef.data = NULL;
  pt->wd = rc;
  pt->length = -1;
  pt->size = 0;
  pt->buffer = NULL;
  return pt;
}

/* destroys resources of the given watched ptags 'pt' */
static void
_destroy_ (struct ptags *pt)
{
  inotify_rm_watch (_watchfd_, pt->wd);
  free (pt->buffer);
  free (pt);
}

/****** Concurrency ******************************/
int
ptags_lock ()
{
#if !defined(NOT_MULTITHREAD_SAFE)
  return pthread_mutex_lock (&_lock_);
#else
  return _error_ (ENOTRECOVERABLE);
#endif
}

int
ptags_unlock ()
{
#if !defined(NOT_MULTITHREAD_SAFE)
  return pthread_mutex_unlock (&_lock_);
#else
  return _error_ (ENOTRECOVERABLE);
#endif
}

/****** Validation of tags and values *************/

int
ptags_is_valid (const char *tag)
{
  char c = *tag;
  if (c == '@')
    return 0;
  while (c)
    {
      if (c < 32 || c == 127 || c == '*' || c == '=')
	return 0;
      c = *++tag;
    }
  return 1;
}

int
ptags_is_valid_value (const char *value)
{
  char c = *value;
  while (c)
    {
      if (c < 32 || c == 127)
	return 0;
      c = *++value;
    }
  return 1;
}

/****** Direct accesses by PID *******************/

void
ptags_sync (pid_t pid)
{
  struct ptags *pt;

  if (pid == 0)
    pid = getpid ();
  pt = _pid_2_ptags_ (pid);
  if (pt != NULL)
    pt->length = -1;
  else if (_ptags_.pid == pid)
    _ptags_.length = -1;
}

char *
ptags_read (pid_t pid, size_t * length)
{
  struct ptags *pt;
  char *result;

  pt = _look_pid_ (pid);
  if (pt == NULL)
    return NULL;

  assert (pt->length >= 0);
  result = malloc (1 + pt->length);
  if (result == NULL)
    errno = ENOMEM;
  else
    {
      if (length)
	*length = (size_t) pt->length;
      memcpy (result, pt->buffer, pt->length);
      result[pt->length] = 0;
    }
  return result;
}

int
ptags_has (pid_t pid, const char *tag)
{
  return _all_ (_look_pid_ (pid), 1, 1, &tag);
}

int
ptags_has_all (pid_t pid, int count, const char *const *tags)
{
  return _all_ (_look_pid_ (pid), 1, count, tags);
}

int
ptags_has_any (pid_t pid, int count, const char *const *tags)
{
  return _all_ (_look_pid_ (pid), 0, count, tags);
}

int
ptags_write (pid_t pid, const char *buffer, size_t length)
{
  int fd;
  ssize_t w;
  char filename[TAGSFILE_MAXLENGTH];

  _path_ (filename, pid ? pid : getpid ());
  fd = open (filename, O_WRONLY | O_CLOEXEC);
  if (fd < 0)
    return fd;
  while (length)
    {
      w = write (fd, buffer, length);
      if (w < 0 && errno != EINTR)
	{
	  close (fd);
	  ptags_sync (pid);
	  return -1;
	}
      if (w > 0)
	{
	  buffer += w;
	  length -= w;
	}
    }
  close (fd);
  ptags_sync (pid);
  return 0;
}

int
ptags_change (pid_t pid, int nradd, const char *const *adds,
	      int nrsub, const char *const *subs)
{
  int i;
  char *buffer, *p;
  size_t length;

  /* computes the length */
  length = 0;
  if (adds != NULL)
    for (i = 0; i < nradd; i++)
      {
	if (strchr (adds[i], '\n'))
	  return _error_ (EINVAL);
	length += 2 + strlen (adds[i]);
      }
  if (subs != NULL)
    for (i = 0; i < nrsub; i++)
      {
	if (strchr (subs[i], '\n'))
	  return _error_ (EINVAL);
	length += 2 + strlen (subs[i]);
      }

  /* check the size */
  if (0 > (ssize_t) length)
    return _error_ (E2BIG);

  /* fun with stack */
  buffer = alloca (length);

  /* build the buffer */
  p = buffer;
  for (i = 0; i < nradd; i++)
    {
      *p++ = '+';
      p = stpcpy (p, adds[i]);
      *p++ = '\n';
    }
  for (i = 0; i < nrsub; i++)
    {
      *p++ = '-';
      p = stpcpy (p, subs[i]);
      *p++ = '\n';
    }

  /* please be true */
  assert (buffer + length == p);

  /* write it now */
  return ptags_write (pid, buffer, length);
}

int
ptags_printf (pid_t pid, const char *format, ...)
{
  int n;
  va_list ap;
  char *buffer;

  va_start(ap, format);
  n = vasprintf(&buffer, format, ap);
  va_end(ap);
  if (n < 0)
    {
      errno = ENOMEM;
      return -1;
    }
  if (n > 0)
    n = ptags_write(pid, buffer, (size_t)n);
  free(buffer);
  return n;
}

int
ptags_add (pid_t pid, const char *tag)
{
  return ptags_printf (pid, "+%s\n", tag);
}

int
ptags_add_keep_flag (pid_t pid, const char *tag)
{
  return ptags_printf (pid, "+@%s\n", tag);
}

int
ptags_add_keep_flag_if_exist (pid_t pid, const char *tag)
{
  return ptags_printf (pid, "?%s\n+@%s\n", tag, tag);
}

int
ptags_sub (pid_t pid, const char *tag)
{
  return ptags_printf (pid, "-%s\n", tag);
}

int
ptags_sub_keep_flag (pid_t pid, const char *tag)
{
  return ptags_printf (pid, "-@%s\n", tag);
}

int
ptags_sub_all (pid_t pid)
{
  return ptags_printf (pid, "-\n");
}

int
ptags_sub_all_keep_flag (pid_t pid)
{
  return ptags_printf (pid, "-@\n");
}

int
ptags_set (pid_t pid, const char *tag, const char *value)
{
  return ptags_printf (pid, "!%s=%s\n", tag, value ? : "");
}

/****** Direct accesses by PID *******************/

int
ptags_watchfd ()
{
  return _watch_ ();
}

int
ptags_watch_sync ()
{
  int rc, pos, n;
  struct ptags *pt;
  char buffer[512];
  struct inotify_event *ie;
  struct
  {
    void *next;
    struct ptags *pt;
  } *nl, *il;

  /* check the state of the watch */
  if (_watchfd_ == 0)
    return 0;

  /* watch active, do the sync */
  nl = NULL;
  for (;;)
    {
      /* read the notifications */
      do
	{
	  rc = read (_watchfd_, buffer, sizeof buffer);
	}
      while (rc < 0 && errno == EINTR);
      if (rc < 0)
	{
	  if (nl != NULL)
	    break;
	  return -(errno != EAGAIN);
	}

      /* invalidate the notified ptags */
      pos = 0;
      while (pos < rc)
	{
	  ie = (struct inotify_event *) &buffer[pos];
	  pt = _wd_2_ptags_ (ie->wd);
	  if (pt != NULL)
	    {
	      for (il = nl; il != NULL && il->pt != pt; il = il->next);
	      if (il == NULL)
		{
		  il = alloca (sizeof *il);
		  il->next = nl;
		  il->pt = pt;
		  nl = il;
		}
	    }
	  pos += ie->len + sizeof *ie;
	}
    }

  /* count and notify */
  n = 0;
  while (nl != NULL)
    {
      pt = nl->pt;
      nl = nl->next;
      /* calls the notification callback if set */
      pt->length = -1;
      if (pt->cbdef.function != NULL)
	pt->cbdef.function (pt->wd, pt->pid, pt->cbdef.data);
      n++;
    }
  return n;
}

int
ptags_watch_get (pid_t pid)
{
  struct ptags *pt;

  if (pid == 0)
    pid = getpid ();

  pt = _pid_2_ptags_ (pid);
  if (pt != NULL)
    pt->use++;
  else
    {
      pt = _create_ (pid);
      if (pt == NULL)
	{
	  return -1;
	}
      pt->next = _ptags_.next;
      _ptags_.next = pt;
    }
  return pt->wd;
}

int
ptags_watch_put (int handle)
{
  struct ptags *pt, *prv;

  prv = &_ptags_;
  pt = _ptags_.next;
  while (pt != NULL && pt->wd != handle)
    {
      prv = pt;
      pt = pt->next;
    }
  if (pt == NULL)
    return _error_ (EBADF);
  pt->use--;
  if (pt->use == 0)
    {
      prv->next = pt->next;
      _destroy_ (pt);
    }
  return 0;
}

pid_t
ptags_watch_pid (int handle)
{
  struct ptags *pt = _wd_2_ptags_ (handle);
  return pt != NULL ? pt->pid : (pid_t) _error_ (EBADF);
}

struct ptags_cbdef
ptags_watch_notify (int handle, struct ptags_cbdef cbdef)
{
  struct ptags_cbdef result;
  struct ptags *pt = _wd_2_ptags_ (handle);
  if (pt == NULL)
    {
      result.function = NULL;
      result.data = NULL;
      errno = EBADF;
    }
  else
    {
      result = pt->cbdef;
      pt->cbdef = cbdef;
    }
  return result;
}

int
ptags_watch_has (int handle, const char *tag)
{
  return _all_ (_look_wd_ (handle), 1, 1, &tag);
}

int
ptags_watch_has_all (int handle, int count, const char *const *tags)
{
  return _all_ (_look_wd_ (handle), 1, count, tags);
}

int
ptags_watch_has_any (int handle, int count, const char *const *tags)
{
  return _all_ (_look_wd_ (handle), 0, count, tags);
}
