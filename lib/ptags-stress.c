#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <poll.h>
#include <fcntl.h>

#include "ptags.h"

char **tags;
pid_t *pids;


int
main (int ac, char **av)
{
  int nrproc, nrtag, i, j;
  pid_t p;
  char buffer[100];

  /* nr proc */
  nrproc = av[1] ? atoi (av[1]) : 0;
  nrproc = nrproc > 0 ? nrproc : 100;

  /* nr tag */
  nrtag = av[1] && av[2] ? atoi (av[2]) : 0;
  nrtag = nrtag > 0 ? nrtag : 100;

  /* make tags */
  tags = malloc (nrtag * sizeof (char *));
  for (i = 0; i < nrtag; i++)
    {
      sprintf (buffer, "stress:tag.%d:%.*s", i, (int) (1 + (15 & rand ())),
	       "0123456789abcdef");
      tags[i] = strdup (buffer);
    }

  /* make processes */
  ptags_add (0, "@ptags:stress:others");
  ptags_add (0, "@ptags:stress:add");
  ptags_add (0, "@ptags:stress:sub");
  pids = malloc (nrproc * sizeof (pid_t));
  pids[0] = getpid ();
  for (i = 1; i < nrproc; i++)
    {
      rand ();
      p = fork ();
      if (p == 0)
	break;
      pids[i] = p;
    }
  nrproc = i;

  /* stress it */
#if defined(PTAGS_FILE_PATTERN)
  sprintf (buffer, PTAGS_FILE_PATTERN, (int) getpid ());
  creat (buffer, 0644);
#endif
  for (;;)
    {
      i = rand () % nrproc;
      j = rand () % nrtag;
      if (rand () & 1)
	ptags_add (pids[i], tags[j]);
      else
	ptags_sub (pids[i], tags[j]);
    }
}
