/*
MIT License

Copyright (c) 2016  jobol@nonadev.net

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#if !defined(_PTAGS_H_)
#define _PTAGS_H_

/*
 * Ptags library is not reentrant.
 * Thus, threads must lock accesses to ptags library
 * if concurrency access to it exist.
 *
 * The function ptags_lock locks the access while ptags_unlock
 * unlock it.
 * These functions return 0 on success. See manual of functions
 * pthread_mutex_lock and pthread_mutex_unlock to get meaning of
 * returned error codes.
 */
extern int ptags_lock ();
extern int ptags_unlock ();

/*
 * Checks wether 'tag' is a valid tag.
 *
 * Returns 1 if 'tag' is valid , 0 otherwise.
 */
extern int ptags_is_valid_tag (const char *tag);

/*
 * Checks wether 'value' is a valid value.
 *
 * Returns 1 if 'value' is valid , 0 otherwise.
 */
extern int ptags_is_valid_value (const char *value);

/*
 * If ptags exists for the process of 'pid' (or self if 'pid'==0),
 * these ptags are invalidated. This has the consequence that
 * next requests for 'pid' will reload the ptags from the process
 * ptags file.
 */
extern void ptags_sync (pid_t pid);

/*
 * Returns the ptags for the process of 'pid' (or self if 'pid'==0).
 *
 * The returned string is the list of tags, each tag terminated
 * with '\n'. The string is null terminated.
 *
 * If a not NULL value is provided for 'length', it will receive
 * the length of the string (without the leading null).
 * If there is no tags, an empty string is returned.
 *
 * The caller must free the returned value.
 *
 * In case of error, NULL is returned and errno is set.
 *
 * errno:
 *  - EFBIG      ptags file too big
 *  - ENOMEM     out of memory
 *  - EILSEQ     bad sequence of character
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 */
extern char *ptags_read (pid_t pid, size_t * length);

/*
 * Query if the current ptags for the process of 'pid' (or self
 * if 'pid'==0) has or not some or all of the given set of tags.
 *
 * 'ptags_has' matches a tag set reduced to the single 'tag'.
 *
 * 'ptags_has_all' matches if all the 'tags' (of 'count') are in
 * the current ptags of the process.
 *
 * 'ptags_has_any' matches if at least one of the 'tags' (of 'count')
 * is in the current ptags of the process.
 *
 * To enforce the read of the ptags for the process of 'pid'
 * before the match, call 'ptags_sync(pid)'.
 * 
 * Return 1 if it matches, 0 if it does not match or -1 in case
 * of error, meaning that the true answer is not available.
 *
 * errno:
 *  - EFBIG      ptags file too big
 *  - ENOMEM     out of memory
 *  - EILSEQ     bad sequence of character
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *
 * See also ptags_sync.
 */
extern int ptags_has (pid_t pid, const char *tag);
extern int ptags_has_all (pid_t pid, int count, const char *const *tags);
extern int ptags_has_any (pid_t pid, int count, const char *const *tags);

/*
 * Writes the 'buffer' of 'length' to the ptags file of the
 * process of 'pid' (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_printf.
 */
extern int ptags_write (pid_t pid, const char *buffer, size_t length);

/*
 * Writes the 'format' of arguments to the ptags file of the
 * process of 'pid' (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - ENOMEM     out of memory
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_write.
 */
extern int ptags_printf (pid_t pid, const char *format, ...) __attribute__ ((format (printf, 2, 3)));

/*
 * Adds 'tag' to the process of 'pid' (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_sub, ptags_add_keep_flag.
 */
extern int ptags_add (pid_t pid, const char *tag);

/*
 * Set the keep flag of 'tag' and creates 'tag' if needed
 * to tags of the process of 'pid' (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_sub, ptags_add_keep_flag.
 */
extern int ptags_add_keep_flag (pid_t pid, const char *tag);

/*
 * Set the keep flag of 'tag' but doesn't create 'tag'
 * to tags of the process of 'pid' (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_sub, ptags_add_keep_flag.
 */
extern int ptags_add_keep_flag_if_exist (pid_t pid, const char *tag);

/*
 * Subs 'tag' from the process of 'pid' (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_write, ptags_add.
 */
extern int ptags_sub (pid_t pid, const char *tag);

/*
 * Removes the keep flag of 'tag' from the process
 * of 'pid' (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_write, ptags_add.
 */
extern int ptags_sub_keep_flag (pid_t pid, const char *tag);

/*
 * Subs all possible tags from the process of 'pid'
 * (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_write, ptags_add.
 */
extern int ptags_sub_all (pid_t pid);

/*
 * Subs all possible keep flags from the process of 'pid'
 * (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_write, ptags_add.
 */
extern int ptags_sub_all_keep_flag (pid_t pid);

/*
 * Attach the 'value' to the 'tag' for the process of 'pid'
 * (or self if 'pid'==0).
 *
 * This action is submitted to restrictions and only effective
 * under condition.
 *
 * The ptags for the changed process will become invalidated
 * as if ptags_sync was called.
 *
 * Returns 0 in case of success. Returns -1 in case of error
 * and set errno.
 *
 * errno:
 *  - EINVAL     a tag is invalid
 *  - E2BIG      memory used for tags is too big
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *  - EPERM      operation not permitted
 *
 * See also: ptags_sync, ptags_write, ptags_add.
 */
extern int ptags_set (pid_t pid, const char *tag, const char *value);

/*
 * Get the file descriptor that is used for watching changes.
 *
 * The returned file descriptor can be watched using poll or
 * select family of functions. It would allow detection of a
 * change in the watched processes and thus a need to call
 * the function 'ptags_watch_sync'.
 *
 * If watching is not started it will be started. This means
 * that an error can occur in that case.
 *
 * Returns either the file descriptor or -1 and set errno in
 * case of error.
 *
 * errno:
 *  - ENOMEM     out of memory
 *  - EMFILE     too many files opened
 *
 * See also: ptags_watch_sync
 */
extern int ptags_watchfd ();

/*
 * Checks the watched processes and invalidate the recorded
 * ptags if needed such that further queries for this
 * processes wil reload the fresh ptags values.
 *
 * Calls the notification callbacks if any for the
 * modified watched processes.
 *
 * Return 0 or -1 in case of error.
 *
 * See also: ptags_watch_notify
 */
extern int ptags_watch_sync ();

/*
 * Get a new watch instance for the process of 'pid'
 * returns its handle.
 *
 * Returns a positive or null value for the handle for the
 * watched proccess or -1 in case of error.
 *
 * errno:
 *  - ENOMEM     out of memory
 *  - EMFILE     too many files opened
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - ENOSPC     no more space for watching
 *
 * See also: ptags_watch_put
 */
extern int ptags_watch_get (pid_t pid);

/*
 * Releases on watch instance for the watched process of 'handle'.
 *
 * Return 0 or -1 in case of error.
 *
 * errno:
 *  - EBADF      bad watch handle 'handle'
 *
 * See also: ptags_watch_get
 */
extern int ptags_watch_put (int handle);

/*
 * Get the pid of the watched process of 'handle'.
 *
 * errno:
 *  - EBADF      bad watch handle 'handle'
 *
 * See also: ptags_watch_get
 */
extern pid_t ptags_watch_pid (int handle);

/*
 * Query if the current ptags for the watched process of 'handle'
 * has or not some or all of the given set of tags.
 *
 * 'ptags_watch_has' matches a tag set reduced to the single 'tag'.
 *
 * 'ptags_watch_has_all' matches if all the 'tags' (of 'count') are in
 * the current ptags of the watched process.
 *
 * 'ptags_watch_has_any' matches if at least one of the 'tags' (of 'count')
 * is in the current ptags of the watched process.
 *
 * To enforce the read of the ptags for the process of 'pid'
 * before the match, call 'ptags_watch_sync()'.
 * 
 * Return 1 if it matches, 0 if it does not match or -1 in case
 * of error, meaning that the true answer is not available.
 *
 * errno:
 *  - EBADF      bad watch handle 'handle'
 *  - EFBIG      ptags file too big
 *  - ENOMEM     out of memory
 *  - EILSEQ     bad sequence of character
 *  - EACCES     access to ptags not allowed
 *  - ENOENT     no ptags files exist
 *  - EMFILE     too many files opened
 *
 * See also ptags_watch_sync, ptags_watch_get.
 */
extern int ptags_watch_has (int handle, const char *tag);
extern int ptags_watch_has_all (int handle, int count,
				const char *const *tags);
extern int ptags_watch_has_any (int handle, int count,
				const char *const *tags);

/*
 * structure used for recording notification callbacks
 */
struct ptags_cbdef
{
  /* the function to be called if not NULL */
  void (*function) (int handle, pid_t pid, void *data);
  /* the data argument to pass to function */
  void *data;
};

/*
 * Set the notification callback for the process of 'handle'.
 *
 * The notification function will be called from the function
 * ptags_watch_sync as soon as a change is detected.
 *
 * Returns the previous values of notification callback
 * or { NULL, NULL } if handle is invalid.
 *
 * The only way to check that there is no error is to set
 * errno to 0 before calling the function. If after calling
 * it errno is set to EBADF, it means that the given handle
 * is invalid and that the returned value is { NULL, NULL }.
 *
 * See also: ptags_watch_sync
 */
extern struct ptags_cbdef ptags_watch_notify (int handle,
					      struct ptags_cbdef cbdef);

#endif
