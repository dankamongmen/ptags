PTAGS
=====

This is the home for the Linux Security Module PTags.

The name *PTags* is used for *Process Tags*.

It contains:

 - the patches to apply on kernels 4.4, 4.8 and 4.9

 - a support library and tools for using **LSM tags**.

 - out of kernel code for developement, test and validation

